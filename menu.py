# !/usr/bin/env python3

__author__ = "Maciej Palubicki"
__copyright__ = "Copyright (c) Maciej Palubicki"
__email__ = "palubickimaciej5@gmail.com"
__version__ = "1.0"

from game_table import GameTable


class Menu:
    def __init__(self):
        print("Witaj w grze Memory wpisz start aby zacząć grę lub wyjdz aby ją opuścić: ")

    def check_comand(self, com: str):
        game = GameTable()
        if com == "start":
            game.time_start()
            game.print_board()
            game.chose()
        elif com == "wyjdz":
            exit()
        else:
            com = input("Nie znam takiej komendy podaj poprawną: ")
