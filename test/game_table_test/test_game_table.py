# !/usr/bin/env python3

__author__ = "Maciej Palubicki"
__copyright__ = "Copyright (c) Maciej Palubicki"
__email__ = "palubickimaciej5@gmail.com"
__version__ = "1.0"

import unittest
from unittest import TestCase

import numpy

from game_table import GameTable


class GameTableTest(TestCase):
    def setUp(self) -> None:
        self.game = GameTable()

    def test_check_answer_right(self):
        self.game.empty_board = list("A#B#############")
        self.game.board = list("A#B#############")
        self.game.empty_board = numpy.array(self.game.empty_board).reshape(4, 4)
        self.game.board = numpy.array(self.game.empty_board).reshape(4, 4)
        self.assertTrue(self.game.check_answer(1, 1, 1, 1))

    def test_check_answer_false(self):
        self.game.empty_board = list("A#D#############")
        self.game.board = list("A#C#############")
        self.game.empty_board = numpy.array(self.game.empty_board).reshape(4, 4)
        self.game.board = numpy.array(self.game.empty_board).reshape(4, 4)
        self.assertFalse(self.game.check_answer(1, 1, 1, 3))

    def test_check_win_right(self):
        self.game.empty_board = list("AABBCCDDEEFFGGHH")
        self.game.empty_board = numpy.array(self.game.empty_board).reshape(4, 4)
        self.assertTrue(self.game.check_win())

    def test_check_win_false(self):
        self.game.empty_board = list("#ABBCCDDEEFFGGHH")
        self.game.empty_board = numpy.array(self.game.empty_board).reshape(4, 4)
        self.assertFalse(self.game.check_win())


if __name__ == "__main__":
    unittest.main()
