# !/usr/bin/env python3

__author__ = "Maciej Palubicki"
__copyright__ = "Copyright (c) Maciej Palubicki"
__email__ = "palubickimaciej5@gmail.com"
__version__ = "1.0"

import os
import random
import time

import numpy


class GameTable:
    def __init__(self):
        self.time = 0
        self.board = list("AABBCCDDEEFFGGHH")
        self.empty_board = list("################")
        random.shuffle(self.board)
        self.board = numpy.array(self.board).reshape(4, 4)
        self.empty_board = numpy.array(self.empty_board).reshape(4, 4)

    def time_start(self):
        self.time = time.time()

    def print_board(self):
        for i in range(0, 4):
            for j in range(0, 4):
                print(self.empty_board[i][j], end=" ")
            print()

    def chose(self):
        line1 = input("Podaj nr wiersza pierwszego elementu jakiego chcesz odkryc: ")
        column1 = input("Podaj nr kolumny pierwszego elementu jakiego chcesz odkryc: ")

        if int(line1) > 4 or int(column1) > 4:
            print("Błędny adress pierwszej komórki spróbuj jescze raz!\n")
            self.print_board()
            self.chose()

        while self.empty_board[int(line1) - 1][int(column1) - 1] != "#":
            print("Wybrano podaną już wartość")
            line1 = input("Podaj nr wiersza pierwszego elementu jakiego chcesz odkryc: ")
            column1 = input("Podaj nr kolumny pierwszego elementu jakiego chcesz odkryc: ")

        self.empty_board[int(line1) - 1][int(column1) - 1] = self.board[int(line1) - 1][int(column1) - 1]
        self.print_board()

        line2 = input("Podaj nr wiersza drugiego elementu jakiego chcesz odkryc: ")
        column2 = input("Podaj nr kolumny drugiego elementu jakiego chcesz odkryc: ")

        if int(line2) > 4 or int(column2) > 4:
            print("Błędny adress drugiej komórki spróbuj jescze raz!\n")
            self.print_board()
            self.chose()

        while self.empty_board[int(line2) - 1][int(column2) - 1] != "#":
            print("Wybrano podaną już wartość")
            line2 = input("Podaj nr wiersza drugiego elementu jakiego chcesz odkryc: ")
            column2 = input("Podaj nr kolumny drugiego elementu jakiego chcesz odkryc: ")

        self.empty_board[int(line2) - 1][int(column2) - 1] = self.board[int(line2) - 1][int(column2) - 1]
        self.print_board()

        if int(line1) > 4 or int(column1) > 4 or int(line2) > 4 or int(column2) > 4:
            print("Błędny adress komórki spróbuj jescze raz!\n")
            self.print_board()
            self.chose()

        if self.check_win():
            end_time = time.time()
            print("Gratulacje udało ci się ukończyć grę. twoj czas wynosi: {} s".format(end_time - self.time))
            win_input = input("\n Wciśnij enter aby wyjść: ")
        else:
            pass
        if self.check_answer(int(line1), int(column1), int(line2), int(column2)):
            print("Poprawna odpowiedz! \n")
        else:
            print("Zla odpowiedz! \n")
            time.sleep(3)

        os.system("CLS")
        self.print_board()
        self.chose()


    def check_answer(self, line1: int, column1: int, line2: int, column2: int):
        if self.board[int(line1) - 1][int(column1) - 1] == self.board[int(line2) - 1][int(column2) - 1]:
            self.empty_board[int(line1) - 1][int(column1) - 1] = self.board[int(line1) - 1][int(column1) - 1]
            self.empty_board[int(line2) - 1][int(column2) - 1] = self.board[int(line2) - 1][int(column2) - 1]
            return True
        else:
            self.empty_board[int(line1) - 1][int(column1) - 1] = "#"
            self.empty_board[int(line2) - 1][int(column2) - 1] = "#"
            return False

    def check_win(self):
        check_sum = 0
        for i in range(0, 4):
            for j in range(0, 4):
                if self.empty_board[i][j] != "#":
                    check_sum = check_sum + 1
        if check_sum == 16:
            return True
        else:
            return False
